using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ControlJugador : MonoBehaviour
{
    [Header("Movimiento")]
    public float smoothTime = 0.1f;
    private Rigidbody rb;
    private Vector3 moveDirection;

    [SerializeField] private float moveSpeed = 5f;

    [Header("Jump")]
    public float jumpCD;
    [SerializeField] private float jumpForce = 10f;
    bool puedeSalto;

    [Header("Dash")]
    public float dashVelocidad;
    bool puedeDash;
    public int contDash;
    public int maximosDash;
    public float dashCD;

    [Header("Sliding")]
    public float slideSpeed;
    public float escalaAgachadoY;
    private float startEscalaY;
    public bool puedeSlide;
    public float slideCD;

    [Header("Teclas de Acceso")]
    public KeyCode Dash = KeyCode.LeftShift;
    public KeyCode Slide = KeyCode.LeftControl;
    public KeyCode Jump = KeyCode.Space;
    public KeyCode Reset = KeyCode.R;

    [Header("Textos")]
    public TMPro.TMP_Text textoGanaste;

    private Vector3 currentVelocity = Vector3.zero;

    private bool isGrounded;
    public bool grappling;

    
    
    private bool sliding;


    //interfaz 
    
    //animator del gameObject
    Animator animator;

    //imagenes de dash
    public GameObject imgDashActivo1;
    public GameObject imgDashActivo2;
    public GameObject imgDashDesactivo1;
    public GameObject imgDashDesactivo2;

    
    //animator del slide
    public Animator animSlide;



    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        startEscalaY = transform.localScale.y;
        puedeSalto = true;
        puedeDash = true;
        puedeSlide = true;
        textoGanaste.text = "";

        animator = GetComponent<Animator>();
        sliding = false;
    }

    private void Update()
    {
        
        Inputs();


        //interfaz de dashes
       InterfazDashes();


        //animaciones de la interfaz del slide
        InterfazSlide();
    }

    private void Inputs()
    {

        if (!isGrounded && puedeDash && (contDash < maximosDash))
        {
            if (Input.GetKeyDown(Dash))
            {
                // Verificar si el jugador quiere dash
                Dashing();
                contDash++;
                Invoke(nameof(ResetDash), dashCD);

            }
        }
        // Verificar si el jugador est� en el suelo
        if (isGrounded && puedeSalto)
        {
            // Verificar si el jugador quiere saltar
            if (Input.GetKey(Jump))
            {
                puedeSalto = false;
                rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                isGrounded = false;
                Invoke(nameof(ResetJump), jumpCD);
            }
        }

        if (Input.GetKeyDown(Slide) && puedeSlide)
        {
            puedeSlide = false;
            Sliding();

            Invoke(nameof(ResetSlide), slideCD);
        }
        else if (Input.GetKeyUp(Slide))
        {
            UpSlide();
        }

        if (Input.GetKeyDown(Reset))
        {
            rb.transform.position = new Vector3(0f, 1f, 0f);
        }
    }

    private void FixedUpdate()
    {
        // Obtener entrada horizontal y vertical
        float moveX = Input.GetAxisRaw("Horizontal");
        float moveZ = Input.GetAxisRaw("Vertical");
     
        // Crear un vector de movimiento a lo largo del eje horizontal y vertical
        moveDirection = new Vector3(moveX, 0f, moveZ).normalized;

       
        
        //animacion correr/quedarse quieto
        if (!sliding)
        {
            if (moveX != 0 || moveZ != 0)
            {
                animator.SetBool("MovimientoActivo", true);
            }
            else
            {
                animator.SetBool("MovimientoActivo", false);
            }
        }
        else
        {
            animator.SetBool("MovimientoActivo", false);
        }



        //// mover rb con .MovePosition
        ///
        //rb.MovePosition(rb.position + transform.TransformDirection(moveDirection) * moveSpeed * Time.fixedDeltaTime);

        rb.MovePosition(rb.position + Vector3.SmoothDamp(rb.velocity, transform.TransformDirection(moveDirection) * moveSpeed, ref currentVelocity, smoothTime) * Time.fixedDeltaTime);
    }

    private void Dashing()
    {
        rb.AddForce(transform.forward * dashVelocidad, ForceMode.Impulse);
        rb.AddForce(transform.up * 0.1f, ForceMode.Impulse);
    }

    private void Sliding()
    {
        transform.localScale = new Vector3(transform.localScale.x, escalaAgachadoY, transform.localScale.z);
        rb.AddForce(transform.forward * slideSpeed, ForceMode.VelocityChange);
        sliding = true;
    }

    private void UpSlide()
    {
        transform.localScale = new Vector3(transform.localScale.x, startEscalaY, transform.localScale.z);
        sliding = false;
    }

    private void ResetJump()
    {
        puedeSalto = true;
    }

    private void ResetDash()
    {
        puedeDash = true;
        contDash --;
    }
    private void ResetSlide()
    {
        puedeSlide = true;
    }

    private void InterfazDashes()
    {

        if (contDash == 1)
        {
            imgDashActivo1.SetActive(true);
            imgDashActivo2.SetActive (false);

        }
        else if (contDash == 2)
        {
            imgDashActivo1.SetActive (false);
            imgDashActivo2.SetActive(false);
        }
        else
        {
            imgDashActivo1.SetActive  (true);
            imgDashActivo2.SetActive (true);
        }
    }

    private void InterfazSlide()
    {
        if (!puedeSlide)
        {
            //UISlideCargando
            animSlide.SetBool("slideCargando",true);
        }
        else
        {
            //UISlideLista
            animSlide.SetBool("slideCargando", false);
        }
    }
    private void OnCollisionEnter(Collision other)
    {
        // Verificar si el jugador est� en el suelo
        if (other.gameObject.CompareTag("Piso"))
        {
            isGrounded = true;
        }
        if (other.gameObject.CompareTag("Meta") == true)
        {
            textoGanaste.text = "Ganaste!!!";
        }
    }

    private void OnCollisionExit(Collision other)
    {
        // Verificar si el jugador ya no est� en el suelo
        if (other.gameObject.CompareTag("Piso"))
        {
            isGrounded = false;
        }
    }

    
}

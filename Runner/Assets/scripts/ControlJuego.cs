using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlJuego : MonoBehaviour
{


    //321 run!
    public TMPro.TMP_Text txt321Run;

    //jugador
    public GameObject jugador;
    private void Start()
    {

        txt321Run.gameObject.SetActive(false);
        if (jugador.transform.position== new Vector3(3.3599999f, -0.187000006f, -1.85000002f))
        {
            txt321Run.gameObject.SetActive(true);
            txt321Run.text = "3";
            InvokeRepeating("cambiarTxt321", 1, 1);
        }
    }

    private void cambiarTxt321()
    {
        string num = txt321Run.text;

        if (num == "3")
        {
            txt321Run.text = "2";
        }
        else if (num == "2")
        {
            txt321Run.text = "1";
        }
        else if (num == "1")
        {
            txt321Run.text = "Run!";
        }
        else
        {
            txt321Run.gameObject.SetActive(false);
            CancelInvoke("cambiarTxt321"); 
        }

      
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

   
}

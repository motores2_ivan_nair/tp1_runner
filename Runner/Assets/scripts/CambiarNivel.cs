using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class CambiarNivel : MonoBehaviour
{
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CambiarEscena(string escena)
    {
        SceneManager.LoadScene(escena);
        
        if (escena == "Parte1_Runner")
        {
            Cursor.lockState = CursorLockMode.Locked;
            
        }

        if (escena == "salir")
        {
            CerrarJuego();
        }
    }

    public void CerrarJuego()
    {
        Application.Quit();
    }
}

using UnityEngine;

public class EnemyAnimationController : MonoBehaviour
{
    public Transform enemy;
    public float minDistance = 5f;
    public float maxDistance = 20f;
    public float maxSpeed = 2f;
    public float minSpeed = 0.5f;

    private Animator animator;

    private void Start()
    {
        animator = enemy.GetComponent<Animator>();
    }

    private void Update()
    {
        Vector3 directionToEnemy = enemy.position - transform.position;
        float distanceToEnemy = directionToEnemy.magnitude;

        // Si el enemigo est� lo suficientemente lejos, reduce la velocidad de la animaci�n.
        if (distanceToEnemy > maxDistance)
        {
            animator.speed = Mathf.Lerp(animator.speed, minSpeed, Time.deltaTime);
        }
        else
        {
            animator.speed = Mathf.Lerp(animator.speed, maxSpeed, Time.deltaTime);
        }
    }
}
